<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="datenschutzerklarung">
        <div class="wrapper">
            <h1 class="header-main u-marginBottom-big u-marginLeft-h1">
                Daten-schutzerklärung
            </h1>

            <div class="container">
                <div class="container-left">
                    <div class="container-left">
                        <p><strong>Kontakt zu der</strong><br>
                            <strong>Weimann &amp; Meyer GbR:</strong></p>
                        <p>Rechtsanwälte<br>
                            Axel Weimann, Dr. Peter Meyer</p>
                        <p>Kurfürstenstraße 56<br>
                            D-10785 Berlin<br>
                            T: +49-(0)30- 254 200-0<br>
                            <a href="#">mail@weimann-meyer.de</a></p>
                    </div>
                </div>
                <div class="container-right">
                        <p>Wir nehmen Datenschutz ernst. Der Schutz Ihrer Privatsphäre bei der Verarbeitung persönlicher Daten ist für uns ein wichtiges Anliegen. Wir behandeln Ihre Daten vertraulich und in Übereinstimmung mit den Vorgaben der in der Bundesrepublik Deutschland geltenden datenschutzrechtlichen Bestimmungen und der berufs- und strafrechtlichen Verschwiegenheitsvorschriften.</p>
                        <p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich. Deshalb bieten wir Ihnen die Möglichkeit einer verschlüsselten E-Mail-Kommunikation im S/MIME- oder PGP-Verschlüsselungsverfahren oder aber unter Generierung eines individuellen Passwortes an. Wir weisen ausdrücklich darauf hin, dass ein von Ihnen ausgehender Erstkontakt mit uns, z.B. durch Verwendung der direkten E-Mail-Kontaktaufnahme von unserem Internetauftritt aus, unverschlüsselt erfolgt und raten deshalb davon ab, bereits in einer solchen Kontaktaufnahme sensible Daten zu versenden. Wir empfehlen Ihnen entweder die direkte telefonische Kontaktaufnahme oder aber uns – d.h. dem jeweiligen gewünschten Ansprechpartner – per E-Mail unter Mitteilung Ihrer telefonischen Erreichbarkeit eine Rückrufbitte zu senden. Dieser Hinweis gilt auch für die Inanspruchnahme des von uns angebotenen Strafverteidiger-Notdienstes.</p>
                        <p>Unabhängig davon gilt grundsätzlich folgendes:</p>
                        <p>Personenbezogene Daten:</p>
                        <p>Personenbezogene Daten sind Daten über Ihre Person. Diese beinhalten Ihren Namen, Ihre Adresse und Ihre E-Mail-Adresse. Sie müssen keine personenbezogenen Daten preisgeben, um unsere Webseite besuchen zu können. In einigen Fällen benötigen wir Ihren Namen und Ihre Adresse sowie weitere Informationen, um Ihnen die gewünschte Dienstleistung anbieten zu können.</p>
                        <p>Wenn Sie einen unserer Services nutzen, sammeln wir in der Regel nur die Daten, die notwendig sind, um Ihnen unseren Service bieten zu können. Möglicherweise fragen wir Sie nach weiteren Informationen, die aber freiwilliger Natur sind. Wann immer wir personenbezogene Daten verarbeiten, tun wir dies, um Ihnen unseren Service anbieten zu können oder um unsere kommerziellen Ziele zu verfolgen. Soweit Sie uns per E-Mail oder über ein Formular (z.B. Rückrufbitte) kontaktieren, verwenden wir die von Ihnen übermittelten personenbezogenen Daten nur für die Bearbeitung Ihrer konkreten Anfrage. Ihre Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.</p>
                        <p>Automatisch gespeicherte, nicht personenbezogene Daten: Wenn Sie unsere Internetseite besuchen, speichern unsere Webserver standardmäßig die IP Ihres Internet Service Provider, die Webseite, von der aus Sie uns besuchen, die Seiten, die Sie bei uns besuchen sowie das Datum und die Dauer des Besuches. Diese Informationen sind für die technische Übertragung der Webseite und den sicheren Serverbetrieb zwingend erforderlich. Eine personalisierte Auswertung dieser Daten erfolgt nicht.</p>
                        <p>Außerdem werden unter Umständen aus administrativen und technischen Gründen bestimmte Informationen gespeichert:</p>
                        <ul>
                            <li>Browsertyp und Browserversion</li>
                            <li>verwendetes Betriebssystem</li>
                            <li>Referrer URL</li>
                            <li>Hostname des zugreifenden Rechners</li>
                            <li>Uhrzeit der Serveranfrage</li>
                        </ul>
                        <p>Diese Daten werden anonymisiert und lediglich für statistische Zwecke bzw. dafür verwendet, unsere Internet- und Onlinedienste zu verbessern. Diese anonymisierten Daten werden – getrennt von personenbezogenen Daten – auf sicheren Systemen gespeichert und können keinen individuellen Personen zugeordnet werden. Das bedeutet, dass Ihre personenbezogenen Daten jederzeit geschützt bleiben.</p>
                        <p>Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen. Wir behalten uns vor, diese Daten nachträglich zu prüfen, wenn uns konkrete Anhaltspunkte für eine rechtswidrige Nutzung bekannt werden.</p>
                </div>

            </div>



        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
