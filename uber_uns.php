<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="uber_uns">
        <div class="wrapper">
            <div class="banner"></div>
            <div class="banner--mobile"></div>
            <div class="row"></div>

            <section class="top">
                <div class="uber_uns__container">
                    <h1 class="header-main u-marginBottom-bigger">
                        Über uns
                    </h1>
                    <p class="uber_uns__text">
                        <span>Weimann &amp; Meyer ist der Zusammenschluss von</span>
                        Rechtsanwältinnen und Rechtsanwälten, die es sich zum Ziel gesetzt haben, Rechtsratsuchenden die persönliche Beratung und Betreuung zu bieten, die sie erwarten dürfen, wenn sie sich an einen Spezialisten wenden. Die im Jahr 1997 gegründete Kanzlei ist nahezu ausschließlich auf den Gebieten Arbeitsrecht und Strafrecht tätig. Ihre Expertise ist anerkannt. WEIMANN &amp; MEYER wird im FOCUS-Spezial "Deutschlands Top-Anwälte" in der Liste "Top-Wirtschaftskanzlei 2013 - Fachgebiet Arbeitsrecht" als eine von 35 Kanzleien deutschlandweit geführt. Dr. Peter Meyer zählte bereits 2004 im Ranking des FOCUS zu den 150 empfohlenen Experten im Arbeitsrecht in Deutschland. Derzeit gehören zu WEIMANN &amp; MEYER neben den beiden Namenspartnern dre weitere Berufsträger.
                    </p>
                    <div class="uber_uns__wrapper u_mt--1">
                        <p class="uber_uns__partner u-marginBottom-small">Sie brauchen ein Kompetenten Partner?</p>
                        <a href="#" class="btn ">Jetzt den Termin vereinbaren<span>→</span></a>
                    </div>
                </div>
            </section>

            <section class="uber_uns__section">
                <h2 class="header-second u-marginBottom-small">
                    Arbeitsrecht
                </h2>
                <div class="uber_uns__container">
                    <div class="uber_uns__wrapper u-marginBottom-big">
                        <div style="background-image: url(images/uber_uns1.jpg)" class="uber_uns__img"></div>
                        <div class="uber_uns__description">
                            <div class="slide-title">Dr. Peter Meyer
                                <span class="slide-title--sub">Rechtsanwalt </span>
                                <span class="slide-title--sub">Fachanwalt für Arbeitsrecht </span>
                            </div>
                            <p class="slide-desc">
                                Peter Meyer wurde am 18. April 1962 in Rietberg/Westfalen geboren.Nach dem Abitur in 1981 und der Ableistung des Grundwehrdienstes begann er im Sommersemester 1983 das Studium der Rechtswissenschaft in Heidelberg...
                            </p>
                        </div>

                    </div>
                    <div class="uber_uns__wrapper u-marginBottom-big">
                        <div style="background-image: url(images/uber_uns2.jpg)" class="uber_uns__img"></div>
                        <div class="uber_uns__description">
                            <div class="slide-title">Anneka Ruwolt
                                <span class="slide-title--sub">Rechtsanwalt </span>
                                <span class="slide-title--sub">Fachanwalt für Arbeitsrecht </span>
                            </div>
                            <p class="slide-desc">
                                Anneka Ruwolt wurde am 28. Mai 1969 in Hamburg geboren. Nach dem Abitur ging sie zunächst für ein Jahr nach Frankreich und übernahm auf einem Gutshof in der Chevennes die Leitung eines Teilbetriebes...
                            </p>
                        </div>
                    </div>
                    <div class="uber_uns__wrapper u-marginBottom-big">
                        <div style="background-image: url(images/uber_uns3.jpg)" class="uber_uns__img"></div>
                        <div class="uber_uns__description">
                            <div class="slide-title">Ulrike Kolb
                                <span class="slide-title--sub">Rechtsanwalt </span>
                                <span class="slide-title--sub">Fachanwalt für Arbeitsrecht </span>
                            </div>
                            <p class="slide-desc">
                                Ulrike Kolb wurde am 30. Januar 1973 in Berlin geboren. Nach der Schule absolvierte sie eine Berufsausbildung zum Elektromonteur. Die Ausbildung schloss sie mit gutem Erfolg ab. Zeitgleich legte sie 1992 das Abitur ab...
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="uber_uns__section">
                <h2 class="header-second u-marginBottom-small">
                    Strafrecht
                </h2>
                <div class="uber_uns__container">
                    <div class="uber_uns__wrapper u-marginBottom-big">
                        <div style="background-image: url(images/uber_uns4.jpg)" class="uber_uns__img"></div>
                        <div class="uber_uns__description">
                            <div class="slide-title">Axel Weimann
                                <span class="slide-title--sub">Fachanwalt für Strafrecht </span>
                            </div>
                            <p class="slide-desc">
                                Axel Weimann wurde am 10. Februar 1963 in Berlin geboren. Nach dem Abitur 1981 folgten das Studium der Rechtswissenschaft an der Freien Universität Berlin und die Referendarzeit im Kammergerichtsbezirk...
                            </p>
                        </div>

                    </div>
                    <div class="uber_uns__wrapper u-marginBottom-big">
                        <div style="background-image: url(images/uber_uns5.jpg)" class="uber_uns__img"></div>
                        <div class="uber_uns__description">
                            <div class="slide-title">Burak Aydin
                                <span class="slide-title--sub">Rechtsanwalt </span>
                            </div>
                            <p class="slide-desc">
                                Burak Aydin wurde am 26. Dezember 1985 in Salzgitter geboren. Nach dem Abitur absolvierte er das Studium der Rechtswissenschaften in Osnabrück und Hannover. Den juristischen Vorbereitungsdienst durchlief er im Oberlandesgerichtsbezirk...
                            </p>
                        </div>
                    </div>
                    <div class="uber_uns__wrapper u-marginBottom-big">
                        <div style="background-image: url(images/uber_uns6.jpg)" class="uber_uns__img"></div>
                        <div class="uber_uns__description">
                            <div class="slide-title">Ulrich Lehmann, LL.M.
                                <span class="slide-title--sub">Rechtsanwältin </span>
                                <span class="slide-title--sub">Fachanwältin für Arbeitsrecht</span>
                            </div>
                            <p class="slide-desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...
                            </p>
                        </div>
                    </div>
                </div>
            </section>


            <section class="uber_uns__section">
                <h2 class="header-second u-marginBottom-small">
                    Büropersonal
                </h2>
                <div class="uber_uns__container">
                    <div class="uber_uns__wrapper u-marginBottom-big">
                        <div style="background-image: url(images/uber_uns7.jpg)" class="uber_uns__img"></div>
                        <div class="uber_uns__description">
                            <div class="slide-title u-colorText">Christin Delf
                                <span class="slide-title--sub">Sekretariat</span>
                            </div>
                            <p class="slide-desc">
                                Axel Weimann wurde am 10. Februar 1963 in Berlin geboren. Nach dem Abitur 1981 folgten das Studium der Rechtswissenschaft an der Freien Universität Berlin und die Referendarzeit im Kammergerichtsbezirk...
                            </p>
                        </div>

                    </div>
                    <div class="uber_uns__wrapper u-marginBottom-big">
                        <div style="background-image: url(images/uber_uns8.jpg)" class="uber_uns__img"></div>
                        <div class="uber_uns__description">
                            <div class="slide-title u-colorText">Dorrit Böttcher
                                <span class="slide-title--sub">Sekretariat</span>
                            </div>
                            <p class="slide-desc">
                                Burak Aydin wurde am 26. Dezember 1985 in Salzgitter geboren. Nach dem Abitur absolvierte er das Studium der Rechtswissenschaften in Osnabrück und Hannover. Den juristischen Vorbereitungsdienst durchlief er im Oberlandesgerichtsbezirk...
                            </p>
                        </div>
                    </div>
                </div>
            </section>




        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>



</div>

<?php include 'includes/footer.php'; ?>
