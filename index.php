<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="main u-marginBottom-huge">
        <div class="wrapper">
            <h1 class="header-main u-marginBottom-big">
                Weimann & Meyer ist der Zusammenschluss von Rechtsanwältinnen und Rechtsanwälten
            </h1>
            <div class="btn-wrapper">
                <a href="#" class="btn">Strafrecht<span>&rarr;</span></a>
                <a href="#" class="btn">Arbeitsrecht<span>&rarr;</span></a>
            </div>
            <div class="row">
                <div class="paragraph">
                    <p class="paragraph__heading">Zusammen sind wir stark.</p>
                    <p class="paragraph__text">Die im Jahr 1997 gegründete Kanzlei ist nahezu ausschließlich auf den Gebieten Arbeitsrecht und Strafrecht tätig. Ihre Expertise ist anerkannt.
                        <a class="text-link" href="#">Mehr</a></p>

                </div>
            </div>

            <div class="swiper-container row swiper-1">
                <div class="swiper-pagination swiper-pagination--left"></div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="swiper-img"  style="background-image: url(images/person.jpg)"></div>
                        <div class="slide-title">Axel Weimann <span class="slide-title--sub">Fachanwalt für Strafrecht</span></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img" style="background-image: url(images/person2.jpg)"></div>
                        <div class="slide-title">Dr. Peter Meyer <span class="slide-title--sub">Rechtsanwalt  |  Fachanwalt für Arbeitsrecht</span></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img"  style="background-image: url(images/person.jpg)"></div>
                        <div class="slide-title">Axel Weimann <span class="slide-title--sub">Fachanwalt für Strafrecht</span></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img" style="background-image: url(images/person2.jpg)"></div>
                        <div class="slide-title">Dr. Peter Meyer <span class="slide-title--sub">Rechtsanwalt  |  Fachanwalt für Arbeitsrecht</span></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img"  style="background-image: url(images/person.jpg)"></div>
                        <div class="slide-title">Axel Weimann <span class="slide-title--sub">Fachanwalt für Strafrecht</span></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img" style="background-image: url(images/person2.jpg)"></div>
                        <div class="slide-title">Dr. Peter Meyer <span class="slide-title--sub">Rechtsanwalt  |  Fachanwalt für Arbeitsrecht</span></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="swiper-testimonial swiper-2">
                    <h2 class="testimonial__heading">Referenzen</h2>
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="testimonial__text">
                               <p>
                                   “Kluge Köpfe gibt es unter Notaren und Anwälten so einige. Wenn sich dieses verbindet mit fachlicher Bandbreite, menschlicher Integrität und exzellenter
                                   Dienstleistung – auch durch ein sympathisches und fähiges Backoffice – , hat man einen perfekten Partner gefunden.”
                               </p>
                            </div>
                            <p class="testimonial__author">Thomas, zazaz Munich</p>
                        </div>
                        <div class="swiper-slide">
                            <div class="testimonial__text">
                                <p>
                                    “Kluge Köpfe gibt es unter Notaren und Anwälten so einige. Wenn sich dieses verbindet mit fachlicher Bandbreite, menschlicher Integrität und exzellenter
                                    Dienstleistung – auch durch ein sympathisches und fähiges Backoffice – , hat man einen perfekten Partner gefunden.”
                                </p>
                            </div>
                            <p class="testimonial__author">Thomas, zazaz Munich</p>
                        </div>
                        <div class="swiper-slide">
                            <div class="testimonial__text">
                                <p>
                                    “Kluge Köpfe gibt es unter Notaren und Anwälten so einige. Wenn sich dieses verbindet mit fachlicher Bandbreite, menschlicher Integrität und exzellenter
                                    Dienstleistung – auch durch ein sympathisches und fähiges Backoffice – , hat man einen perfekten Partner gefunden.”
                                </p>
                            </div>
                            <p class="testimonial__author">Thomas, zazaz Munich</p>
                        </div>


                    </div>
                    <div class="swiper-pagination swiper-pagination--right"></div>
                </div>

            </div>


        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
