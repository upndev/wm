<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="contacts">
        <div class="wrapper">
            <h1 class="header-main u-marginBottom-big u-marginLeft-h1">
                Kontakt
            </h1>

            <div class="container">
                <div class="contacts-left">


                        <p><b>WEIMANN &amp; MEYER<br>
                                Rechtsanwälte</b></p>
                        <p>Kurfürstenstraße 56<br>
                            D-10785 Berlin<br>
                            Google Maps</p>
                        <p>Tel. +49-(0)30- 254 200-0<br>
                            Fax. +49-(0)30- 254 200-25</p>
                        <div class="contacts__email">
                            <i id="contacts-icon"></i>
                            <p>E-Mail Sekretariat:<br>
                                mail@weimann-meyer.de</p>
                        </div>
                        <p><a href="https://www.facebook.com/">E-Mail an Axel Weimann</a><br>
                            <a href="https://www.facebook.com/">E-Mail an Dr. Peter Meyer</a><br>
                            <a href="https://www.facebook.com/">E-Mail an Ulrike Kolb</a><br>
                            <a href="https://www.facebook.com/">E-Mail an Anneka Ruwolt</a><br>
                            <a href="https://www.facebook.com/">E-Mail an Burak Aydin</a></p>
                </div>
                <div class="contacts-right">
                    <iframe class="contacts__map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2428.7505075757763!2d13.35361281581573!3d52.501755679810785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47a8504c454b5b87%3A0xbf427077939d461e!2sKurf%C3%BCrstenstra%C3%9Fe+56%2C+10785+Berlin%2C+Vokietija!5e0!3m2!1slt!2slt!4v1522918364085" frameborder="0" style="border:0" allowfullscreen></iframe></iframe>
                </div>

            </div>



        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
