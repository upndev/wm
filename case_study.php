<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="case_study">
        <div class="wrapper">
            <h1 class="header-main u-marginLeft-h1">
                Entlassung von 3.000 Mitarbeitern
            </h1>

            <div class="container">
                <div class="container-left">
                    <h2 class="header-block">Project team</h2>
                    <ul class="left__list">
                        <li class="left__item">
                            <div class="left__image" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns1.jpg);">
                                <a href="http://wm.upndev.com/dr-peter-meyer/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/dr-peter-meyer/">
                                    Dr. Peter Meyer                          	</a>
                                <span>Lawyer</span>
                                <span>Specialist lawyer for employment law</span>
                            </div>
                        </li>
                        <li class="left__item">
                            <div class="left__image" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns2.jpg)">
                                <a href="http://wm.upndev.com/anneka-ruwolt/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/anneka-ruwolt/">
                                    Anneka Ruwolt                          	</a>
                                <span>Lawyer</span>
                                <span>Specialist lawyer for employment law</span>
                            </div>
                        </li>
                        <li class="left__item">
                            <div class="left__image" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns3.jpg)">
                                <a href="http://wm.upndev.com/ulrike-kolb/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/ulrike-kolb/">
                                    Ulrike Kolb                          	</a>
                                <span>Lawyer</span>
                                <span>Specialist lawyer for employment law</span>
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="container-right">
                    <div class="container-right__img" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/case_study.png)"></div>
                    <p class="container-right__text">Aus unserer täglichen Beratungspraxis kennen wir die typischen Unterschiede zwischen dem norwegischen und insbesondere dem deutschen Arbeits- und Sozialversicherungsrecht. Wir können daher unsere Mandanten im Vorfeld auf diese Unterschiede hinweisen und Arbeitsverträge so gestalten, dass sie auch im norwegischen arbeits und sozialversicherungsrechtlichen System die Vorgaben aus Deutschland, Österreich und der Schweiz erfüllen.</p>
                    <p class="container-right__text">Aus unserer täglichen Beratungspraxis kennen wir die typischen Unterschiede zwischen dem norwegischen und insbesondere dem deutschen Arbeits- und Sozialversicherungsrecht. Wir können daher unsere Mandanten im Vorfeld auf diese Unterschiede hinweisen und Arbeitsverträge so gestalten, dass sie auch im norwegischen arbeits- und sozialversicherungsrechtlichen System die Vorgaben aus Deutschland, Österreich und der Schweiz erfüllen.</p>
                    <p class="container-right__text">Aus unserer täglichen Beratungspraxis kennen wir die typischen Unterschiede zwischen dem norwegischen und insbesondere dem deutschen Arbeits- und Sozialversicherungsrecht. Wir können daher unsere Mandanten im Vorfeld auf diese Unterschiede hinweisen und Arbeitsverträge so gestalten, dass sie auch im norwegischen arbeits- und sozialversicherungsrechtlichen System die Vorgaben aus Deutschland, Österreich und der Schweiz erfüllen. Aus unserer täglichen Beratungspraxis kennen wir die typischen Unterschiede zwischen dem norwegischen und insbesondere dem deutschen Arbeits- und Sozialversicherungsrecht. Wir können daher unsere Mandanten im Vorfeld auf diese Unterschiede hinweisen und Arbeitsverträge so gestalten, dass sie auch im norwegischen arbeits- und sozialversicherungsrechtlichen System die Vorgaben aus Deutschland, Österreich und der Schweiz erfüllen.</p>
                    <p class="container-right__text">Aus unserer täglichen Beratungspraxis kennen wir die typischen Unterschiede zwischen dem norwegischen und insbesondere dem deutschen Arbeits- und Sozialversicherungsrecht. Wir können daher unsere Mandanten im Vorfeld auf diese Unterschiede hinweisen und Arbeitsverträge so gestalten, dass sie auch im norwegischen arbeits- und sozialversicherungsrechtlichen System die Vorgaben aus Deutschland, Österreich und der Schweiz erfüllen.</p>
                    <div class="btn-wrapper">
                        <a href="http://wm.upndev.com/dr-peter-meyer/" class="nav-link">Next</a>
                        <span class="separator">|</span>
                        <a href="http://wm.upndev.com/dr-peter-meyer/" class="nav-link">Previous</a>

                    </div>

                </div>

            </div>



        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
