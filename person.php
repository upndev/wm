<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="person">
        <div class="wrapper">
            <div class="mobileImg" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns4.jpg"></div>
            <h1 class="header-main u-marginLeft-h1 u-marginBottom-bigger">
                Axel Weimann
                <span class="header-main--sub">Fachanwalt für Strafrecht</span>
            </h1>

            <div class="container">
                <div class="container-left">
                    <a href="tel:+490302542000" class="person__contacts">
                        <i class="icon">
                            <svg class="phone-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="2645.1 38.1 580.5 580.5"><g><g><path  d="M3086.5,422.2c-19-18.7-42.6-18.7-61.5,0c-14.4,14.3-28.8,28.5-42.9,43c-3.9,4-7.1,4.8-11.8,2.2c-9.3-5.1-19.2-9.2-28.1-14.7c-41.7-26.2-76.6-59.9-107.5-97.8c-15.3-18.8-29-39-38.5-61.7c-1.9-4.6-1.6-7.6,2.2-11.4c14.4-13.9,28.4-28.1,42.5-42.4c19.7-19.8,19.7-43-0.1-62.9c-11.2-11.4-22.5-22.5-33.7-33.8c-11.6-11.6-23.1-23.3-34.8-34.8c-19-18.5-42.6-18.5-61.5,0.1c-14.5,14.3-28.4,28.9-43.1,42.9c-13.7,12.9-20.5,28.8-22,47.2c-2.3,30.1,5.1,58.5,15.5,86.1c21.3,57.3,53.6,108.1,92.9,154.7c53,63.1,116.3,112.9,190.4,148.9c33.3,16.2,67.9,28.6,105.5,30.7c25.8,1.5,48.3-5.1,66.3-25.2c12.3-13.8,26.2-26.3,39.3-39.5c19.3-19.6,19.4-43.2,0.2-62.6C3132.6,468.1,3109.5,445.1,3086.5,422.2z"/><path  d="M3063.4,325.9l44.6-7.6c-7-41-26.3-78-55.7-107.5c-31-31-70.3-50.6-113.6-56.7l-6.3,44.8c33.5,4.7,63.9,19.8,87.9,43.9C3043.1,265.5,3058,294.2,3063.4,325.9z"/><path  d="M3133.1,132.1c-51.5-51.5-116.6-84-188.5-94l-6.3,44.8c62.1,8.7,118.4,36.8,162.8,81.2c42.2,42.2,69.8,95.4,79.9,154l44.6-7.6C3213.9,242.6,3181.9,181,3133.1,132.1z"/></g></g></svg>

                        </i>
                        +49-(0)30-254 2000
                    </a>
                    <a href="tel:+490302542000" class="person__contacts">
                        <i class="icon">
                            <svg class="mail-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="1527.8 105.8 550.8 432.8" enable-background="new 1527.8 105.8 550.8 432.8" xml:space="preserve"><g><g><path d="M2029.4,538.5c12.4,0,23.1-4.1,32.2-12.2l-156-156.1c-3.7,2.7-7.4,5.3-10.8,7.8c-11.7,8.6-21.2,15.3-28.4,20.1c-7.3,4.8-17,9.7-29,14.8c-12.1,5-23.4,7.5-33.8,7.5h-0.3h-0.3c-10.5,0-21.7-2.5-33.8-7.5c-12.1-5-21.8-9.9-29-14.8c-7.3-4.8-16.8-11.5-28.4-20.1c-3.3-2.4-6.9-5-10.8-7.8l-156.1,156.1c9.1,8.1,19.9,12.2,32.2,12.2H2029.4z"/><path  d="M1558.8,272c-11.7-7.8-22-16.7-31-26.7v237.4l137.5-137.5C1637.8,326,1602.3,301.6,1558.8,272z"/><path  d="M2047.8,272c-41.9,28.3-77.4,52.7-106.8,73.2l137.5,137.5V245.3C2069.7,255.1,2059.5,264.1,2047.8,272z"/><path  d="M2029.4,105.8h-452.4c-15.8,0-27.9,5.3-36.4,16c-8.5,10.7-12.8,24-12.8,40c0,12.9,5.6,26.9,16.9,42c11.3,15.1,23.3,26.9,36,35.5c7,4.9,28,19.5,63,43.8c18.9,13.1,35.4,24.5,49.5,34.4c12,8.4,22.4,15.7,31,21.7c1,0.7,2.5,1.8,4.6,3.3c2.2,1.6,5,3.6,8.5,6.1c6.7,4.8,12.2,8.7,16.6,11.7c4.4,3,9.7,6.3,16,10c6.2,3.7,12.1,6.5,17.7,8.3c5.5,1.8,10.7,2.8,15.4,2.8h0.3h0.3c4.7,0,9.8-0.9,15.4-2.8c5.5-1.8,11.4-4.6,17.7-8.3c6.2-3.7,11.6-7,16-10c4.4-3,9.9-6.9,16.6-11.7c3.5-2.5,6.3-4.5,8.5-6.1c2.1-1.5,3.6-2.6,4.6-3.3c6.7-4.7,17.1-11.9,31.1-21.6c25.5-17.7,63-43.8,112.7-78.3c15-10.4,27.5-23.1,37.5-37.8c10-14.7,15.1-30.2,15.1-46.4c0-13.5-4.9-25.1-14.6-34.7C2054.2,110.6,2042.7,105.8,2029.4,105.8z"/></g></g></svg>
                        </i>
                        +49-(0)30-254 2000
                    </a>
                    <a href="#" class="person__contacts">
                        <i class="icon">
                            <svg class="mail-icon" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1344 1344q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm256 0q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm128-224v320q0 40-28 68t-68 28h-1472q-40 0-68-28t-28-68v-320q0-40 28-68t68-28h465l135 136q58 56 136 56t136-56l136-136h464q40 0 68 28t28 68zm-325-569q17 41-14 70l-448 448q-18 19-45 19t-45-19l-448-448q-31-29-14-70 17-39 59-39h256v-448q0-26 19-45t45-19h256q26 0 45 19t19 45v448h256q42 0 59 39z"/></svg>

                        </i>
                        Download vCard
                    </a>




<!--                    <div class="iconsHolder">-->
<!--                        <a href="#" class="iconLink">-->
<!--                            <svg class="socialIcon" viewBox="0 0 24 24">-->
<!--                                <path d="M21,21H17V14.25C17,13.19 15.81,12.31 14.75,12.31C13.69,12.31 13,13.19 13,14.25V21H9V9H13V11C13.66,9.93 15.36,9.24 16.5,9.24C19,9.24 21,11.28 21,13.75V21M7,21H3V9H7V21M5,3A2,2 0 0,1 7,5A2,2 0 0,1 5,7A2,2 0 0,1 3,5A2,2 0 0,1 5,3Z"/>-->
<!--                            </svg>-->
<!--                        </a>-->
<!--                        <span class="vLine"></span>-->
<!--                        <a href="#" class="nav-link vCard-link">Download vCard</a>-->
<!--                    </div>-->
                </div>

                <div class="container-right">
                    <div class="container-right__img" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns4.jpg"></div>
                    <p class="container-right__text">Axel Weimann wurde am 10. Februar 1963 in Berlin geboren. Nach dem Abitur 1981 folgten das Studium der Rechtswissenschaft an der Freien Universität Berlin und die Referendarzeit im Kammergerichtsbezirk.</p>
                    <p class="container-right__text">Nachdem bereits sein Großvater, Vater und Onkel als Strafverteidiger, Staatsanwalt und Richter in der Berliner Strafjustiz tätig waren, hat auch Axel Weimann sich frühzeitig für das Strafrecht entschieden und ist seit seiner Zulassung zur Rechtsanwaltschaft im Jahr 1992 nahezu ausschließlich auf dem Gebiet der Strafverteidigung tätig. Nach ersten gemeinsamen Jahren in einer größeren Berliner Kanzlei gründete Axel Weimann im Jahr 1997 mit Dr. Peter Meyer die heute unter dem Namen Weimann &amp; Meyer von beiden geführte Sozietät. Seit 1998 ist Axel Weimann auch Fachanwalt für Strafrecht. Von Anfang an war er in nahezu allen Bereichen des Strafrechts tätig. Bereits im Jahr seiner Zulassung zur Anwaltschaft übernahm er die Verteidigung in einem Schwurgerichtsverfahren, dem Fall um die Ermordung der Berliner Rotlicht-Größe “Chinesen-Kalle”, das im Revisionsverfahren 1997 mit einem Freispruch endete – und es folgten weitere. Unvergesslich werden ihm die Eindrücke bleiben, die er als Verteidiger in historischen Verfahren wie den sog. “Mauerschützen-Prozessen” und den Prozessen um das “DDR-Doping” sowie die angebliche Erpressung Ausreisewilliger gegen den Ost-Berliner Rechtsanwalt Wolfgang Vogel u.a. und in weiteren Verfahren sog. Regierungs- und Vereinigungskriminalität im Zusammenhang mit der deutsch-deutschen Wiedervereinigung sammeln durfte. Gleiches gilt für die Verteidigertätigkeit in dem um die Jahrtausendwende von der Staatsanwaltschaft Berlin geführten und im Ergebnis eingestellten Ermittlungsverfahren im Zusammenhang mit der Auftragsvergabe zum Bau des Berliner Großflughafens BER.</p>
                    <div id="attachment_83" style="width: 1199px" class="wp-caption alignnone"><img class="wp-image-83 size-full" src="http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns7.jpg" alt="" width="1189" height="791" srcset="http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns7.jpg 1189w, http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns7-300x200.jpg 300w, http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns7-768x511.jpg 768w, http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns7-1024x681.jpg 1024w" sizes="(max-width: 1189px) 100vw, 1189px"><p class="wp-caption-text">Lorem ipsum</p></div>
                    <p class="container-right__text">Seit Jahren liegt der Schwerpunkt der Verteidiger- und Beratertätigkeit von Axel Weimann vor allem im Arztstrafrecht sowie im Wirtschaftsstrafrecht mit all seinen Teilgebieten. Er vertritt Ärzte sowohl gegen den Vorwurf der fahrlässigen Tötung bzw. Körperverletzung in sog. “Kunstfehler-Prozessen”, wie auch in Verfahren wegen sog. Abrechnungsbetruges. Knapp fünfzehn Jahre war Axel Weimann als Verteidiger eines ehemaligen Bankvorstands in zwei Verfahren zum “Berliner Bankenskandal” tätig. Während im sog. “AUBIS-Verfahren” die zunächst erfolgte Verurteilung zu einer Bewährungsstrafe erst auf eine Verfassungsbeschwerde hin vom Bundesverfassungsgericht als verfassungswidrig aufgehoben musste und das Verfahren daraufhin ohne erneute Verhandlung eingestellt wurde, konnte das sog. “Fonds-Verfahren” mit einem Freispruch abgeschlossen werden.</p>
                    <p class="container-right__text">Die Vertretung von Menschen unterschiedlichster Herkunft und Profession führt Axel Weimann aber immer wieder auch in nahezu alle anderen Bereiche des Strafrechts. Aus der Tätigkeit der letzten Jahre sind die Verteidigung eines der Zwillinge, gegen die wegen des Berliner “KaDeWe-Einbruchs” ermittelt worden war, die Verteidigung von “Onur U.” in dem Fall “Jonny K.” sowie die Verteidigung von “Mario K.” im sog. “Maskenmann-Prozess” hervorzuheben. Seit 2001 ist Axel Weimann Mitglied des Vorstands der Rechtsanwaltskammer Berlin und gehört als Vorsitzender der für die Fachanwaltschaften zuständigen Abteilung I des Vorstands auch dem Präsidium der Rechtsanwaltskammer an. Axel Weimann ist Mitglied des Berliner Anwaltsvereins, der Vereinigung Berliner Strafverteidiger e.V. und der Arbeitsgemeinschaft Strafrecht des Deutschen Anwaltvereins.</p>
                    <p class="container-right__text">Als Autor zeichnet Axel Weimann für den strafrechtlichen Teil im “Handbuch Medizinrecht” von Stellpflug/Meier/Tadayon verantwortlich und hat zum selben Thema auch im Fachanwaltskurs für Medizinrecht des Arber-Verlages doziert. Weitere Dozententätigkeit erfolgte im Bereich des Jugendstrafrechts und der Vermögens- und Urkundsdelikte für das Fortbildungsprogramm der Arbeitsgemeinschaft Strafrecht des Deutschen Anwaltvereins.</p>
                    <div class="btn-wrapper">
                        <a href="http://wm.upndev.com/dr-peter-meyer/" class="nav-link">Next</a>
                    </div>
                </div>

            </div>
        </div>
    </main>
<?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
