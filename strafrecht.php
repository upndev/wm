<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="starfrecht">
        <div class="wrapper">
            <h1 class="header-main u-marginBottom-big u-marginLeft-h1">
                Strafrecht
            </h1>

            <div class="container">
                <div class="container-left">
                    <h3 class="header-block">Zuständige Personen</h3>
                    <ul class="left__list">
                        <li class="left__item">
                            <div class="left__image" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns4.jpg)">
                                <a href="http://wm.upndev.com/de/axel-weimann/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/de/axel-weimann/">
                                    Axel Weimann                          	</a>
                                <span></span>
                                <span>Fachanwalt für Strafrecht</span>
                            </div>
                        </li>
                        <li class="left__item">
                            <div class="left__image" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns5.jpg)">
                                <a href="http://wm.upndev.com/de/burak-aydin/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/de/burak-aydin/">
                                    Burak Aydin                          	</a>
                                <span>Rechtsanwalt</span>
                                <span></span>
                            </div>
                        </li>
                        <li class="left__item">
                            <div class="left__image" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns6.jpg)">
                                <a href="http://wm.upndev.com/de/ulrich-lehmann-ll-m/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/de/ulrich-lehmann-ll-m/">
                                    Ulrich Lehmann, LL.M.                          	</a>
                                <span>Rechtsanwältin</span>
                                <span>Fachanwältin für Strafrecht</span>
                            </div>
                        </li>
                    </ul>

                    <h3 class="header-block">Referenzen</h3>
                    <ul class="credentials__list">
                        <li class="credentials__items"><a class="nav-link" href="#">Das Berufsbild des Strafverteidigers</a></li>
                        <li class="credentials__items"><a class="nav-link" href="#">Organisation einer Strafverteidigerkanzlei</a></li>
                        <li class="credentials__items"><a class="nav-link" href="#">Handbuch Medizinrecht</a></li>

                    </ul>
                </div>
                <div class="container-right">
                    <div class="starfrecht__intro">
                        <p>
                            Aus unserer täglichen Beratungspraxis kennen wir die typischen
                            Unterschiede zwischen dem norwegischen und insbesondere dem deutschen Arbeits- und Sozialversicherungsrecht. Wir können daher
                            unsere Mandanten im Vorfeld auf diese Unterschiede hinweisen und
                            Arbeitsverträge so gestalten, dass sie auch im norwegischen arbeits und sozialversicherungsrechtlichen System die Vorgaben aus Deutschland,
                            Österreich und der Schweiz erfüllen.
                        </p>
                        <p><a class="" href="tel:0171-4641555">NOTDIENST-TELEFON-NR: 0171-4641555.</a></p>
                    </div>

                    <h3 class="header-block">Starfrecht Gebiete</h3>
                    <ul class="starfrecht__list">
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Allgemeines Strafrecht</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, quia, veritatis? Alias assumenda consequatur deserunt distinctio eius, eum itaque maxime molestiae nesciunt nobis, officia, quam soluta unde! Adipisci delectus deserunt dicta dignissimos error eveniet explicabo facilis ipsam laborum neque, nihil obcaecati qui quia ratione repudiandae unde velit veniam voluptas voluptatem!
                                </p>
                            </div>


                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Arztstrafrecht</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">

                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, quia, veritatis? Alias assumenda consequatur deserunt distinctio eius, eum itaque maxime molestiae nesciunt nobis, officia, quam soluta unde! Adipisci delectus deserunt dicta dignissimos error eveniet explicabo facilis ipsam laborum neque, nihil obcaecati qui quia ratione repudiandae unde velit veniam voluptas voluptatem!
                                </p>
                            </div>

                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Insolvenzstrafrecht</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, quia, veritatis? Alias assumenda consequatur deserunt distinctio eius, eum itaque maxime molestiae nesciunt nobis, officia, quam soluta unde! Adipisci delectus deserunt dicta dignissimos error eveniet explicabo facilis ipsam laborum neque, nihil obcaecati qui quia ratione repudiandae unde velit veniam voluptas voluptatem!
                                </p>
                            </div>

                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">IT-Strafrecht</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>

                            </div>

                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Jugendstrafrecht</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">

                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi iste laboriosam molestiae odit officia porro sapiente tempore tenetur veniam vitae! A accusamus animi autem consectetur consequatur cum cumque dignissimos dolorum ducimus eaque est eveniet, in iure molestias nam nisi non perspiciatis quae quasi quidem recusandae rem repellat reprehenderit totam, unde! Ab accusamus asperiores autem consectetur deserunt doloremque ducimus earum error esse et ex excepturi exercitationem explicabo, facilis fugiat fugit hic illo, incidunt inventore ipsam, iusto laborum laudantium molestias mollitia nam natus nostrum quae quas quo rerum tempore tenetur ullam velit. Accusantium aliquid aut harum maiores nostrum porro qui. Quia reiciendis sequi veritatis. A alias fugiat neque quia sequi, similique veniam? Ab adipisci alias aliquid beatae blanditiis commodi cupiditate debitis doloribus ea excepturi facere, illum incidunt ipsam iste iusto laboriosam maxime minus modi nemo nostrum numquam odio officia praesentium quaerat quo rerum sapiente sequi temporibus totam unde velit veritatis vero voluptatibus.
                                </p>
                            </div>

                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Kapitalstrafrecht</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Korruptionsstrafrecht</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Medizinstrafrecht</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Ordnungswidrigkeitenrecht</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Präventivberatung</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                                <p class="starfrecht__text">
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                                </p>
                            </div>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Revisionsrecht</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Steuerstrafrecht</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Strafrechtliche Belange des Arbeitsrechts</h2><span class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Umweltstrafrecht</h2><span class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Verkehrsstrafrecht</h2><span class="starfrecht__arrow"></span>
                            <p class="starfrecht__text"></p>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Wirtschaftsstrafrecht</h2><span class="starfrecht__arrow"></span>
                            <p class="starfrecht__text">


                            </p>
                        </li>
                    </ul>



                </div>

            </div>



        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
