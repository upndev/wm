<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="starfrecht">
        <div class="wrapper">
            <h1 class="header-main u-marginBottom-big u-marginLeft-h1">
                Arbeitsrecht
            </h1>

            <div class="container">
                <div class="container-left">
                    <h3 class="header-block u-mobileMB-1">Zuständige Personen</h3>

                    <ul class="left__list">

                        <li class="left__item">
                            <div class="left__image" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns1.jpg)">
                                <a href="http://wm.upndev.com/de/dr-peter-meyer/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/de/dr-peter-meyer/">
                                    Dr. Peter Meyer </a>
                                <span>Lawyer</span>
                                <span>Specialist lawyer for employment law</span>
                            </div>
                        </li>
                        <li class="left__item">
                            <div class="left__image" style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns2.jpg)">
                                <a href="http://wm.upndev.com/de/anneka-ruwolt/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/de/anneka-ruwolt/">
                                    Anneka Ruwolt </a>
                                <span>Rechtsanwalt</span>
                                <span>Fachanwalt für Arbeitsrecht</span>
                            </div>
                        </li>
                        <li class="left__item">
                            <div class="left__image"  style="background-image: url(http://wm.upndev.com/wp-content/uploads/2018/04/uber_uns3.jpg)">
                                <a href="http://wm.upndev.com/de/ulrike-kolb/"></a>
                            </div>
                            <div class="left__employee">
                                <a href="http://wm.upndev.com/de/ulrike-kolb/">
                                    Ulrike Kolb </a>
                                <span>Rechtsanwalt</span>
                                <span>Fachanwalt für Arbeitsrecht</span>
                            </div>
                        </li>

                    </ul>


                    <ul class="left__list">
                        <li class="left__item">
                            <div class="left__image" style="background-image: url(images/lawyer2.jpg)"></div>
                            <div class="left__employee">
                                Dr. Peter Meyer
                                <span>Rechtsanwalt</span>
                                <span>Fachanwalt für Arbeitsrecht</span>
                            </div>
                        </li>

                        <li class="left__item">
                            <div class="left__image" style="background-image: url(images/lawyer2.jpg)"></div>
                            <div class="left__employee">
                                Anneka Ruwolt
                                <span>Rechtsanwalt</span>
                                <span>Fachanwältin für Arbeitsrecht</span>
                            </div>
                        </li>

                        <li class="left__item">
                            <div class="left__image" style="background-image: url(images/lawyer2.jpg)"></div>
                            <div class="left__employee">
                                Ulrike Kolb
                                <span>Rechtsanwalt</span>
                                <span>Fachanwältin für Arbeitsrecht</span>
                            </div>
                        </li>
                    </ul>

                    <h3 class="header-block">Referenzen</h3>
                    <ul class="credentials__list">
                        <li class="credentials__items"><a class="nav-link" href="#">Das Berufsbild des
                                Strafverteidigers</a></li>
                        <li class="credentials__items"><a class="nav-link" href="#">Organisation einer
                                Strafverteidigerkanzlei</a></li>
                        <li class="credentials__items"><a class="nav-link" href="#">Handbuch Medizinrecht</a></li>

                    </ul>
                </div>
                <div class="container-right">
                    <div class="starfrecht__intro">
                        <p>
                            Aus unserer täglichen Beratungspraxis kennen wir die typischen
                            Unterschiede zwischen dem norwegischen und insbesondere dem deutschen Arbeits- und
                            Sozialversicherungsrecht. Wir können daher
                            unsere Mandanten im Vorfeld auf diese Unterschiede hinweisen und
                            Arbeitsverträge so gestalten, dass sie auch im norwegischen arbeits und
                            sozialversicherungsrechtlichen System die Vorgaben aus Deutschland,
                            Österreich und der Schweiz erfüllen.
                        </p>
                    </div>


                    <h3 class="header-block">Arbeitsrecht Gebiete</h3>
                    <ul class="starfrecht__list">
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Beratung und Vertretung von Unternehmen, Geschäftsführern,
                                Vorständen und eitenden Angestellten</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, quia,
                                    veritatis? Alias assumenda consequatur deserunt distinctio eius, eum itaque maxime
                                    molestiae nesciunt nobis, officia, quam soluta unde! Adipisci delectus deserunt
                                    dicta dignissimos error eveniet explicabo facilis ipsam laborum neque, nihil
                                    obcaecati qui quia ratione repudiandae unde velit veniam voluptas voluptatem!
                                </p>
                            </div>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Beratung und Vertretung von Arbeitnehmern und freien
                                Mitarbeitern</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, quia,
                                    veritatis? Alias assumenda consequatur deserunt distinctio eius, eum itaque maxime
                                    molestiae nesciunt nobis, officia, quam soluta unde! Adipisci delectus deserunt
                                    dicta dignissimos error eveniet explicabo facilis ipsam laborum neque, nihil
                                    obcaecati qui quia ratione repudiandae unde velit veniam voluptas voluptatem!
                                </p>
                            </div>

                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Arbeitsrecht des öffentlichen Dienstes und der
                                Hochschulen</h2><span class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, quia,
                                    veritatis? Alias assumenda consequatur deserunt distinctio eius, eum itaque maxime
                                    molestiae nesciunt nobis, officia, quam soluta unde! Adipisci delectus deserunt
                                    dicta dignissimos error eveniet explicabo facilis ipsam laborum neque, nihil
                                    obcaecati qui quia ratione repudiandae unde velit veniam voluptas voluptatem!
                                </p>
                            </div>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Arbeitsrecht der Kirchen und ihrer Einrichtungen</h2><span
                                    class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, deserunt.
                                </p>
                            </div>

                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Arbeitsrecht der Krankenkassen und Krankenhäuser</h2><span
                                    class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi iste laboriosam
                                    molestiae odit officia porro sapiente tempore tenetur veniam vitae! A accusamus
                                    animi autem consectetur consequatur cum cumque dignissimos dolorum ducimus eaque est
                                    eveniet, in iure molestias nam nisi non perspiciatis quae quasi quidem recusandae
                                    rem repellat reprehenderit totam, unde! Ab accusamus asperiores autem consectetur
                                    deserunt doloremque ducimus earum error esse et ex excepturi exercitationem
                                    explicabo, facilis fugiat fugit hic illo, incidunt inventore ipsam, iusto laborum
                                    laudantium molestias mollitia nam natus nostrum quae quas quo rerum tempore tenetur
                                    ullam velit. Accusantium aliquid aut harum maiores nostrum porro qui. Quia
                                    reiciendis sequi veritatis. A alias fugiat neque quia sequi, similique veniam? Ab
                                    adipisci alias aliquid beatae blanditiis commodi cupiditate debitis doloribus ea
                                    excepturi facere, illum incidunt ipsam iste iusto laboriosam maxime minus modi nemo
                                    nostrum numquam odio officia praesentium quaerat quo rerum sapiente sequi temporibus
                                    totam unde velit veritatis vero voluptatibus.
                                </p>
                            </div>

                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Unterstützung von Arbeitgebern in der Personalarbeit</h2>
                            <span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Gestaltung und Begleitung von Arbeits- und
                                Dienstverträgen</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Allgemeines Gleichbehandlungsgesetz</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Einführung und Veränderung von flexiblen Arbeitszeitmodellen
                                und Vergütungsstrukturen</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Betriebliche Altersversorgung</h2><span
                                    class="starfrecht__arrow"></span>
                            <div class="starfrecht__text-block js-toggleList">
                                <p class="starfrecht__text">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                                <p class="starfrecht__text">
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                                    qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste
                                    natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam,
                                    eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta
                                    sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                                    fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi
                                    nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                                    consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut
                                    labore et dolore magnam aliquam quaerat voluptatem.
                                </p>
                            </div>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Arbeitnehmer-Datenschutz und Compliance</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Befristungsrecht</h2><span class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Beendigung von Arbeitsverhältnissen (Kündigung,
                                Aufhebungsvertrag)</h2><span class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Begleitung von Massenentlassungen</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Arbeitsrecht in der Insolvenz</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Betriebsverfassungsrecht</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Personalvertretungsrecht</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Betriebsänderungen, Interessenausgleich und Sozialplan</h2>
                            <span class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Einigungsstellen- und Beschlussverfahren</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Tarifvertragsrecht</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Tarifverhandlungen</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Recht der Handelsvertreter</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>

                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Arbeitnehmerüberlassung und - vermittlung</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Arbeitsförderung</h2><span class="starfrecht__arrow"></span>
                        </li>
                        <li class="starfrecht__item">
                            <h2 class="starfrecht__heading">Inhouse-Schulungen und Vortragstätigkeiten</h2><span
                                    class="starfrecht__arrow"></span>
                        </li>
                    </ul>


                </div>

            </div>


        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
