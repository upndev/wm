<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="case_studies">
        <div class="wrapper">
            <h1 class="header-main u-marginBottom-big">Case studies</h1>
            <section class="galleryBlock">
                <h2 class="gallery_heading">Arbeitsrecht</h2>
                <div class="gallery">
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies1.jpg)"></div>
                            <p class="gallery__img-text">Das Berufsbild des Strafverteidigers</p>
                        </a>

                    </div>
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies2.jpg)"></div>
                            <p class="gallery__img-text">Organisation einer Strafverteidigerkanzlei</p>
                        </a>
                    </div>
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies3.jpg"></div>
                            <p class="gallery__img-text">Handbuch Medizinrecht</p>
                        </a>
                    </div>
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies1.jpg)"></div>
                            <p class="gallery__img-text">Das Berufsbild des Strafverteidigers</p>
                        </a>
                    </div>
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies2.jpg)"></div>
                            <p class="gallery__img-text">Das Berufsbild des Strafverteidigers</p>
                        </a>
                    </div>

                </div>
            </section>
            <section class="galleryBlock">
                <h2 class="gallery_heading">Strafrecht</h2>
                <div class="gallery">
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies1.jpg)"></div>
                            <p class="gallery__img-text">Das Berufsbild des Strafverteidigers</p>
                        </a>

                    </div>
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies2.jpg)"></div>
                            <p class="gallery__img-text">Organisation einer Strafverteidigerkanzlei</p>
                        </a>
                    </div>
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies3.jpg"></div>
                            <p class="gallery__img-text">Handbuch Medizinrecht</p>
                        </a>
                    </div>
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies1.jpg)"></div>
                            <p class="gallery__img-text">Das Berufsbild des Strafverteidigers</p>
                        </a>
                    </div>
                    <div class="gallery__wrapper">
                        <a href="#" class="gallery__link">
                            <div class="gallery__img" style="background-image: url(images/case_studies2.jpg)"></div>
                            <p class="gallery__img-text">Das Berufsbild des Strafverteidigers</p>
                        </a>
                    </div>

                </div>
            </section>



        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
