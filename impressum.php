<?php include 'includes/header.php'; ?>

<div class="wrapper">
    <?php include 'includes/navigation.php'; ?>

    <main class="impressum">
        <div class="wrapper">
            <h1 class="header-main u-marginBottom-big u-marginLeft-h1">
                Impressum
            </h1>

            <div class="container">
                <div class="container-left">
                    <p><strong>Kontakt zu der</strong><br>
                        <strong>Weimann &amp; Meyer GbR:</strong></p>
                    <p>Rechtsanwälte<br>
                        Axel Weimann, Dr. Peter Meyer</p>
                    <p>Kurfürstenstraße 56<br>
                        D-10785 Berlin<br>
                        T: +49-(0)30- 254 200-0<br>
                        <a href="#">mail@weimann-meyer.de</a></p>
                </div>
                <div class="container-right">


                    <p class="container-right__text">
                        Die Rechtsanwälte der Kanzlei Weimann & Meyer sind Rechtsanwälte nach dem Recht der Bundesrepublik Deutschland und Mitglieder der Rechtsanwaltskammer Berlin, Littenstraße 9, 10179 Berlin, Tel.: 030/306931-0. Die einschlägigen berufsrechtlichen Vorschriften (Bundesrechtsanwaltsordnung, Berufsordnung für Rechtsanwälte, Fachanwaltsordnung, Berufsregeln der Rechtsanwälte der Europäischen Gemeinschaft, Rechtsanwaltsvergütungsgesetz) können unter der Rubrik "Für Anwälte - Berufsrecht" auf der Internetseite der Bundesrechtsanwaltskammer eingesehen werden.
                    </p>
                    <p class="container-right__text">
                        Umsatzsteueridentifikationsnummer (§ 27 a UStG) USt-IDNr.: DE189868047 Berufshaftpflichtversicherung Die Berufshaftpflichtversicherung aller bei Weimann & Meyer tätigen Rechtsanwältinnen und Rechtsanwälte besteht bei der AXA Versicherung AG - Niederlassung Berlin, Postfach 12 03 52, 10593 Berlin; Dovestr. 2-4, 10587 Berlin. Der räumliche Geltungsbereich des Versicherungsschutzes umfasst Tätigkeiten in Europa; dabei gelten die entsprechenden Regelungen in Teil 1 A § 4 Ziff. 1, Teil 2 A Ziff. 2.1, 4.1 AVB-RSW bzw. § 51 Abs. 3 Nr. 2 bis 4 BRAO. Abweichend von Teil 2 A Ziff. 2.1 AVB-RSW erstreckt sich der Versicherungsschutz darüber hinaus auch auf Haftpflichtansprüche aus Tätigkeiten
                    </p>

                    <p class="container-right__text">
                        im Zusammenhang mit der Beratung und Beschäftigung mit außereuropäischem Recht, des Rechtsanwalts vor außereuropäischen Gerichten. In Erweiterung von Teil 2 A Ziff. 4.1 AVB-RSW erstreckt sich der Versicherungsschutz auch auf Haftpflichtansprüche aus der Inanspruchnahme vor außereuropäischen Gerichten bis zur Höhe der im Versicherungsvertrag vereinbarten Versicherungssumme. Bei der Inanspruchnahme vor Gerichten von Staaten außerhalb Europas ersetzt der Versicherer Kosten nach der dem Streitwert - maximal der Versicherungssumme - entsprechenden Wertklasse nach Angaben des Rechtsanwaltsvergütungsgesetzes. Der Versicherungsschutz erstreckt sich nicht auf Ansprüche aus Tätigkeiten, die über Kanzleien oder Büros im Ausland ausgeübt werden. Die Aufwendungen des Versicherers für Kosten der gerichtlichen und außergerichtlichen Abwehr der von einem Dritten geltend gemachten Ansprüche, insbesondere Anwalts-, Sachverständigen-, Zeugen- und Gerichtskosten, werden - abweichend von Teil 1 A § 3 III, Ziff. 5 AVB-RSW - als Leistungen auf die Versicherungssumme angerechnet.
                    </p>

                    <p class="container-right__text">
                        <b>Außergerichtliche Streitbeilegung</b>
                    </p>

                    <p class="container-right__text">
                        Die Adresse der Plattform der EU zur außergerichtlichen Online-Streitbeilegung lautet: http://ec.europa.eu/consumers/odr/.
                        Für vermögensrechtliche Streitigkeiten aus dem Mandatsverhältnis ist die Schlichtungsstelle der Rechtsanwaltschaft, Neue Grünstraße 17, 10179 Berlin, www.s-d-r.org, zuständig.
                    </p>

                    <p class="container-right__text">
                        Wir sind weder bereit noch verpflichtet, an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle oder der Schlichtungsstelle der Rechtsanwaltschaft teilzunehmen.
                    </p>

                    <p class="container-right__text">
                        <b><span class="u-displayBlock">Disclaimer (Haftungsausschluss)</span>
                            Inhalt des Onlineangebotes</b>
                    </p>

                    <p class="container-right__text">
                        Die Kanzlei Weimann & Meyer übernimmt keinerlei Gewähr für die Aktualität, Korrektheit, Vollständigkeit oder Qualität der bereitgestellten Informationen. Haftungsansprüche gegen die Kanzlei, welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens der Kanzlei des Autors kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt.

                    </p>
                    <p class="container-right__text">
                        Alle Angebote sind freibleibend und unverbindlich. Die Kanzlei Weimann & Meyer behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.
                    </p>

                    <p class="container-right__text">
                        <b>Verweise und Links</b>
                    </p>
                    <p class="container-right__text">
                        Bei direkten oder indirekten Verweisen auf fremde Webseiten ("Hyperlinks"), die außerhalb des Verantwortungsbereiches der Kanzlei Weimann & Meyer liegen, würde eine Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem die Autoren von den Inhalten Kenntnis haben und es ihnen technisch möglich und zumutbar wäre, die Nutzung im Falle rechtswidriger Inhalte zu verhindern.
                        Die Autoren erklären hiermit ausdrücklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zukünftige Gestaltung, die Inhalte oder die Urheberschaft der verlinkten Seiten hat die Kanzlei Weimann & Meyer keinerlei Einfluss und distanziert sich daher hiermit ausdrücklich von allen Inhalten aller verlinkten Seiten, die nach der Linksetzung verändert wurden. Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.
                    </p>
                    <p class="container-right__text">
                        <b>Urheber- und Kennzeichenrecht</b>
                    </p>
                    <p class="container-right__text">
                        Die Kanzlei Weimann & Meyer ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihr selbst erstellte Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen.
                    </p>
                    <p class="container-right__text">
                        Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind.
                    </p>
                    <p class="container-right__text">
                        Das Copyright für veröffentlichte, von der Kanzlei Weimann & Meyer selbst erstellte Objekte bleibt allein bei den Autoren der Seiten. Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung der Rechtsanwälte Weimann und Meyer nicht gestattet.
                    </p>

                    <p class="container-right__text">
                        <b>Datenschutz</b>
                    </p>

                    <p class="container-right__text">
                        Die Nutzung der im Rahmen des Impressums oder vergleichbarer Angaben veröffentlichten Kontaktdaten wie Postanschriften, Telefon- und Faxnummern sowie E-Mail-Adressen durch Dritte zur Übersendung von nicht ausdrücklich angeforderten Informationen ist nicht gestattet. Rechtliche Schritte gegen die Versender von sogenannten Spam-Mails bei Verstößen gegen dieses Verbot sind ausdrücklich vorbehalten. Rechtswirksamkeit dieses Haftungsausschlusses
                    </p>
                    <p class="container-right__text">
                        Dieser Haftungsausschluss ist als Teil des Internetangebotes www.weimann-meyer.de zu betrachten. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt.
                        Erstellung der Webseite
                    </p>

                </div>

            </div>



        </div>

    </main>
    <?php include 'includes/footer-nav.php'; ?>


</div>

<?php include 'includes/footer.php'; ?>
